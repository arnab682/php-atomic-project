<?php
namespace App;

Class Mother{

  public $motherFName = null;
  public $motherMName = null;
  public $motherLName = null;
  public $motherSuffix = null;
  public $motherDateOfBirth = null;
  public $motherMarriageFName = null;
  public $motherMarriageMName = null;
  public $motherMarriageLName = null;
  public $motherMarriageSuffix = null;
  public $motherBirthplace = null;
  public $motherResidence = null;
  public $motherCounty = null;
  public $motherLocation = null;
  public $motherStreet = null;
  public $motherApartment = null;
  public $motherZipCode = null;
  public $insideCity = null;

  function __construct($allData)
  {
    echo "<pre><h2>Mother's Details<h2><pre>";
    if(array_key_exists("motherFName",$allData)){
      $this->motherFName = $allData["motherFName"];
    }
    if(array_key_exists("motherMName",$allData)){
      $this->motherMName = $allData["motherMName"];
    }
    if(array_key_exists("motherLName",$allData)){
      $this->motherLName = $allData["motherLName"];
    }
    if(array_key_exists("motherSuffix",$allData)){
      $this->motherSuffix = $allData["motherSuffix"];
    }
    if(array_key_exists("motherDateOfBirth",$allData)){
      $this->motherDateOfBirth = $allData["motherDateOfBirth"];
    }
    if(array_key_exists("motherMarriageFName",$allData)){
      $this->motherMarriageFName = $allData["motherMarriageFName"];
    }
    if(array_key_exists("motherMarriageMName",$allData)){
      $this->motherMarriageMName = $allData["motherMarriageMName"];
    }
    if(array_key_exists("motherMarriageLName",$allData)){
      $this->motherMarriageLName = $allData["motherMarriageLName"];
    }
    if(array_key_exists("motherMarriageSuffix",$allData)){
      $this->motherMarriageSuffix = $allData["motherMarriageSuffix"];
    }
    if(array_key_exists("motherBirthplace",$allData)){
      $this->motherBirthplace = $allData["motherBirthplace"];
    }
    if(array_key_exists("motherResidence",$allData)){
      $this->motherResidence = $allData["motherResidence"];
    }
    if(array_key_exists("motherCounty",$allData)){
      $this->motherCounty = $allData["motherCounty"];
    }
    if(array_key_exists("motherLocation",$allData)){
      $this->motherLocation = $allData["motherLocation"];
    }
    if(array_key_exists("motherStreet",$allData)){
      $this->motherStreet = $allData["motherStreet"];
    }
    if(array_key_exists("motherApartment",$allData)){
      $this->motherApartment = $allData["motherApartment"];
    }
    if(array_key_exists("motherZipCode",$allData)){
      $this->motherZipCode = $allData["motherZipCode"];
    }
    if(array_key_exists("insideCity",$allData)){
      $this->insideCity = $allData["insideCity"];
    }
  }


}


?>