<?php
namespace App;

Class Father{

  public $fatherFName = null;
  public $fatherMName = null;
  public $fatherLName = null;
  public $fatherSuffix = null;
  public $fatherDateOfBirth = null;
  public $fatherBirthplace = null;

  function __construct($allData)
  {
    echo "<pre><h2>Father's Details</h2></pre>";
    if(array_key_exists("fatherFName",$allData)){
      $this->fatherFName = $allData["fatherFName"];
    }
    if(array_key_exists("fatherMName",$allData)){
      $this->fatherMName = $allData["fatherMName"];
    }
    if(array_key_exists("fatherLName",$allData)){
      $this->fatherLName = $allData["fatherLName"];
    }
    if(array_key_exists("fatherSuffix",$allData)){
      $this->fatherSuffix = $allData["fatherSuffix"];
    }
    if(array_key_exists("fatherDateOfBirth",$allData)){
      $this->fatherDateOfBirth = $allData["fatherDateOfBirth"];
    }
    if(array_key_exists("fatherBirthplace",$allData)){
      $this->fatherBirthplace = $allData["fatherBirthplace"];
    }
  }

}


?>