<?php
namespace App;

Class Certifier{

  public $certifierName = null;
  public $certifierTitle = null;
  public $certifierTitleOtherSpecify = null;
  public $dateCertified = null;
  public $dateFieldByRegister = null;
  public $telephone = null;
  public $ur = null;
  public $referringHospital = null;

  function __construct($allData)
  {
    echo "<pre><h2>Certifier</h2></pre>";
    if(array_key_exists("certifierName",$allData)){
      $this->certifierName = $allData["certifierName"];
    }
    if(array_key_exists("certifierTitle",$allData)){
      $this->certifierTitle = $allData["certifierTitle"];
    }
    if(array_key_exists("certifierTitleOtherSpecify",$allData)){
      $this->certifierTitleOtherSpecify = $allData["certifierTitleOtherSpecify"];
    }
    if(array_key_exists("dateCertified",$allData)){
      $this->dateCertified = $allData["dateCertified"];
    }
    if(array_key_exists("dateFieldByRegister",$allData)){
      $this->dateFieldByRegister = $allData["dateFieldByRegister"];
    }
    if(array_key_exists("telephone",$allData)){
      $this->telephone = $allData["telephone"];
    }
    if(array_key_exists("ur",$allData)){
      $this->ur = $allData["ur"];
    }
    if(array_key_exists("referringHospital",$allData)){
      $this->referringHospital = $allData["referringHospital"];
    }
  }

  
}

?>