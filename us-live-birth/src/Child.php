<?php
namespace App;

Class Child{

  public $childFName = null;
  public $childMName = null;
  public $childLName = null;
  public $childSuffix = null;
  public $childTimeOfBirth = null;
  public $childGender = null;
  public $childDateOfBirth = null;
  public $childFacilityName = null;
  public $childLocationBirth = null;
  public $childCountyOfBirth = null;

  function __construct($allData) 
  {
    echo "<pre><h2>Child's Details</h2></pre>";
    if(array_key_exists("childFName",$allData)){
      $this->childFName = $allData["childFName"];
    }
    if(array_key_exists("childMName",$allData)){
      $this->childMName = $allData["childMName"];
    }
    if(array_key_exists("childLName",$allData)){
      $this->childLName = $allData["childLName"];
    }
    if(array_key_exists("childSuffix",$allData)){
      $this->childSuffix = $allData["childSuffix"];
    }
    if(array_key_exists("childTimeOfBirth",$allData)){
      $this->childTimeOfBirth = $allData["childTimeOfBirth"];
    }
    if(array_key_exists("childGender",$allData)){
      $this->childGender = $allData["childGender"];
    }
    if(array_key_exists("childDateOfBirth",$allData)){
      $this->childDateOfBirth = $allData["childDateOfBirth"];
    }
    if(array_key_exists("childFacilityName",$allData)){
      $this->childFacilityName = $allData["childFacilityName"];
    }
    if(array_key_exists("childLocationBirth",$allData)){
      $this->childLocationBirth = $allData["childLocationBirth"];
    }
    if(array_key_exists("childCountyOfBirth",$allData)){
      $this->childCountyOfBirth = $allData["childCountyOfBirth"];
    }
  }

}


?>