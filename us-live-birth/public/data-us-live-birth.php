<?php
 include_once('../vendor/autoload.php');
 
 use Utility\Sanitize;
 use Utility\Validation;
 use Utility\Debugger;
 use Utility\Utility;
 use App\Child; // namespace\ClassName
 use App\Father;
 use App\Certifier;
 use App\Mother;
use Illuminate\Auth\Events\Validated;

$sanitize = new Sanitize();
$validation = new Validation();
//$debugger = new Debugger();
$utility = new Utility();

if($utility->isPosted()) {
    //Incoming Data
    $dataSanitize = $sanitize->sanitize($_POST); //sanitize receive post data
    $dataValidate = $validation->validate($dataSanitize); //validate sanitize data
    $allData = $dataValidate; //store in a variable

 } else {
   exit();
 }

 
 //$debugger->debugger($allData);
//Debugger::debugger($allData);


//  $child->setData($_POST);
//  $father->setData($_POST);
//  $certifier->setData($_POST);
//  $mother->setData($_POST);
//  print_r($_POST);
//  var_dump($_POST);

$child = new Child($allData); // object create for this class

echo(($child->childFName == null) ?  "Null" : $child->childFName)."<br>";
echo(!empty($child->childMName) ? $child->childMName : 'Not Provided')."<br>";

// echo $child->childFName."<br>";
// echo $child->childMName."<br>";
echo $child->childLName."<br>";
echo $child->childSuffix."<br>";
echo $child->childTimeOfBirth."<br>";
echo $child->childGender."<br>";
echo $child->childDateOfBirth."<br>";
echo $child->childFacilityName."<br>";
echo $child->childLocationBirth."<br>";
echo $child->childCountyOfBirth."<br>";
echo "<hr>";

$father = new Father($allData);

echo $father->fatherFName."<br>";
echo $father->fatherMName."<br>";
echo $father->fatherLName."<br>";
echo $father->fatherSuffix."<br>";
echo $father->fatherDateOfBirth."<br>";
echo $father->fatherBirthplace."<br>";
echo "<hr>";

$certifier = new Certifier($allData);

echo $certifier->certifierName."<br>";
echo $certifier->certifierTitle."<br>";
echo $certifier->certifierTitleOtherSpecify."<br>";
echo $certifier->dateCertified."<br>";
echo $certifier->dateFieldByRegister."<br>";
echo $certifier->telephone."<br>";
echo $certifier->ur."<br>";
echo $certifier->referringHospital."<br>";
echo "<hr>";

$mother = new Mother($allData);

echo $mother->motherFName."<br>";
echo $mother->motherMName."<br>";
echo $mother->motherLName."<br>";
echo $mother->motherSuffix."<br>";
echo $mother->motherDateOfBirth."<br>";
echo $mother->motherMarriageFName."<br>";
echo $mother->motherMarriageMName."<br>";
echo $mother->motherMarriageLName."<br>";
echo $mother->motherMarriageSuffix."<br>";
echo $mother->motherBirthplace."<br>";
echo $mother->motherResidence."<br>";
echo $mother->motherCounty."<br>";
echo $mother->motherLocation."<br>";
echo $mother->motherStreet."<br>";
echo $mother->motherApartment."<br>";
echo $mother->motherZipCode."<br>";
echo $mother->insideCity."<br>";


?>