<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style type="text/css">
    body {
      margin: 0%;
      padding: 0%;
    }
    label {
      font-size: 13px;
    }
    section h2{
      text-align: center;
      margin: 20px;
      font-size: 38px;
      color: darkslategrey;
    }
    h2 {
      color: darkslategrey
    }
    p {
      font-size: 20px;
      color: black;
      
    }
    button {
      display: flex;
      align-items: center;
      justify-content: center;
    }
  </style>
</head>
<body>
  <div class="container" >
    <section class="header">
      <h2><u>Us standard certificate of live Birth</u></h2>
    </section>
    <form action="data-us-live-birth.php" method="post">
      <div class="row">
        <div class="col-md-6" style="background-color: ">
          <h2>Child's Details</h2>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="childFName">First Name</label><br>
                <input type="text" class="form-control" name="childFName" id="childFName">
              </div>
              <div class="col-md-6 form-group">
                <label for="childMName">Middle Name</label><br>
                <input type="text" class="form-control" name="childMName" id="childMName">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="childLName">Last Name</label><br>
                <input type="text" class="form-control" name="childLName" id="childLName">
              </div>
              <div class="col-md-6 form-group">
                <label for="childSuffix">Suffix</label><br>
                <input type="text" class="form-control" name="childSuffix" id="childSuffix">
              </div>
            </div>

            <div class="form-group">
              <label for="childTimeOfBirth">Time Of Birth(24 hr)</label><br>
              <input type="email" class="form-control" id="childTimeOfBirth" name="childTimeOfBirth">
            </div>
            
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="childGender">Sex</label><br>
                <select name="childGender" class="form-control" id="childGender">
                  <option value="">Select your gender</option>
                  <option value="0">Male</option>
                  <option value="1">Female</option>
                  <option value="2">Others</option>
                </select>
              </div>

              <div class="col-md-6 form-group">
                <label for="childDateOfBirth">Date of Birth (Mo/Day/Yr)</label><br>
                <input type="date" class="form-control" id="childDateOfBirth" name="childDateOfBirth">
              </div>
            
            </div>
            
            <div class="form-group">
              <label for="childFacilityName">Facility Name (If not institution, give street and number)</label><br>
              <input type="text" class="form-control" id="childFacilityName" name="childFacilityName">
            </div>

            <div class="form-group">
              <label for="childLocationBirth">City, Town or Location of Birth</label><br>
              <input type="text" class="form-control" id="childLocationBirth" name="childLocationBirth">
            </div>

            <div class="form-group">
              <label for="childCountyOfBirth">County of Birth</label><br>
              <input type="text" class="form-control" id="childCountyOfBirth" name="childCountyOfBirth">
            </div>


          <h2>Father's Details</h2>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="fatherFName">First Name</label><br>
                <input type="text" class="form-control" name="fatherFName" id="fatherFName">
              </div>
              <div class="col-md-6 form-group">
                <label for="fatherMName">Middle Name</label><br>
                <input type="text" class="form-control" name="fatherMName" id="fatherMName">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="fatherLName">Last Name</label><br>
                <input type="text" class="form-control" name="fatherLName" id="fatherLName">
              </div>
              <div class="col-md-6 form-group">
                <label for="fatherSuffix">Suffix</label><br>
                <input type="text" class="form-control" name="fatherSuffix" id="fatherSuffix">
              </div>
            </div>
            
            <div class="form-group">
              <label for="fatherDateOfBirth">Date of Birth (Mo/Day/Yr)</label><br>
              <input type="date" class="form-control" id="fatherDateOfBirth" name="fatherDateOfBirth">
            </div>

            <div class="form-group">
              <label for="fatherBirthplace">Birthplace (State,Temitory or Foreign Country)</label><br>
              <input type="text" class="form-control" id="fatherBirthplace" name="fatherBirthplace">
            </div>

          <h2>Certifier's Details</h2>
            <div class="form-group">
              <label for="certifierName">Certifier's Name</label><br>
              <input type="text" class="form-control" id="certifierName" name="certifierName">
            </div>
            <div class="form-group">
              <label id="certifierTitle">Title</label><br>
              <span style="font-size: 14px">
              <input type="checkbox" name="certifierTitleMD" >
              <label for="certifierTitleMD">MD</label>

              <input type="checkbox" name="certifierTitle" id="certifierTitleDo">
              <label for="certifierTitleDo">DO</label>

              <input type="checkbox" name="certifierTitle" id="certifierTitleHA">
              <label for="certifierTitleHA">HOSPITAL ADMIN</label>

              <input type="checkbox" name="certifierTitle" id="certifierTitleCM">
              <label for="certifierTitleCM">CNM/CM</label>

              <input type="checkbox" name="certifierTitle" id="certifierTitleOM">
              <label for="certifierTitleOM">OTHER MIDWIFE</label><br>

              <input type="checkbox" name="certifierTitle" id="certifierTitleOS">
              <label for="certifierTitleOS">OTHER(Specify)</label> </span>
              <input type="text" class="form-control" name="certifierTitleOtherSpecify" id="certifierTitleOtherSpecify">
              
            </div>
              
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="dateCertified">Date Certified(Mo/Day/Yr)</label><br>
                <input type="text" class="form-control" name="dateCertified" id="dateCertified">
              </div>
              <div class="col-md-6 form-group">
                <label for="dateFieldByRegister">Date Field by Register(Mo/Day/Yr)</label><br>
                <input type="text" class="form-control" name="dateFieldByRegister" id="dateFieldByRegister">
              </div>
            </div>

            <div class="form-group">
              <label for="telephone">Telephone</label><br>
              <input type="text" class="form-control" id="telephone" name="telephone">
            </div>

            <div class="form-group">
              <label for="ur">UR#</label><br>
              <input type="text" class="form-control" id="ur" name="ur">
            </div>

            <div class="form-group">
              <label for="referringHospital">Referring Hospital</label><br>
              <input type="text" class="form-control" id="referringHospital" name="referringHospital">
            </div>

        </div>
        
        <div class="col-md-6">
          <h2>Mother's Details</h2>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="motherFName">First Name</label><br>
                <input type="text" class="form-control" name="motherFName" id="motherFName">
              </div>
              <div class="col-md-6 form-group">
                <label for="motherMName">Middle Name</label><br>
                <input type="text" class="form-control" name="motherMName" id="motherMName">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="motherLName">Last Name</label><br>
                <input type="text" class="form-control" name="motherLName" id="motherLName">
              </div>
              <div class="col-md-6 form-group">
                <label for="motherSuffix">Suffix</label><br>
                <input type="text" class="form-control" name="motherSuffix" id="motherSuffix">
              </div>
            </div>

            <div class="form-group">
              <label for="motherDateOfBirth">Date of Birth (Mo/Day/Yr)</label><br>
              <input type="date" class="form-control" id="motherDateOfBirth" name="motherDateOfBirth">
            </div>
              
            <!-- <small class="form-text text-muted">Mother's Name Prior to First Marriage</small> -->
            <p>Mother's Name Prior to First Marriage</p>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="motherMarriageFName">First Name</label><br>
                <input type="text" class="form-control" name="motherMarriageFName" id="motherMarriageFName">
              </div>
              <div class="col-md-6 form-group">
                <label for="motherMarriageMName">Middle Name</label><br>
                <input type="text" class="form-control" name="motherMarriageMName" id="motherMarriageMName">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="motherMarriageLName">Last Name</label><br>
                <input type="text" class="form-control" name="motherMarriageLName" id="motherMarriageLName">
              </div>
              <div class="col-md-6 form-group">
                <label for="motherMarriageSuffix">Suffix</label><br>
                <input type="text" class="form-control" name="motherMarriageSuffix" id="motherMarriageSuffix">
              </div>
            </div>

            <div class="form-group">
              <label for="motherBirthplace">Birthplace (State,Temitory or Foreign Country)</label><br>
              <input type="text" class="form-control" id="motherBirthplace" name="motherBirthplace">
            </div>

            <div class="form-group">
              <label for="motherResidence">Residence of Mother-State</label><br>
              <input type="text" class="form-control" id="motherResidence" name="motherResidence">
            </div>

            <div class="form-group">
              <label for="motherCounty">County</label><br>
              <input type="text" class="form-control" id="motherCounty" name="motherCounty">
            </div>

            <div class="form-group">
              <label for="motherLocation">City, Town or Location</label><br>
              <input type="text" class="form-control" id="motherLocation" name="motherLocation">
            </div>

            <div class="form-group">
              <label for="motherStreet">Street and Number</label><br>
              <input type="text" class="form-control" id="motherStreet" name="motherStreet">
            </div>

            <div class="form-group">
              <label for="motherApartment">Apartment No.</label><br>
              <input type="text" class="form-control" id="motherApartment" name="motherApartment">
            </div>

            <div class="row">
              <div class="col-md-6 form-group">
                <label for="motherZipCode">Zip Code</label><br>
                <input type="text" class="form-control" name="motherZipCode" id="motherZipCode">
              </div>
              <div class="col-md-6 form-group">
                <label>Inside City Limits?</label><br>
                
                <input type="radio" name="insideCity" id="insideCity_Yes" value="1" checked="">
                <label for="insideCity_Yes">Yes</label>
                
                <input type="radio" name="insideCity" id="insideCity_No" value="0">
                <label for="insideCity_No">No</label>
              </div>
            </div>

        </div>
        
      </div>
      <div>
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="reset" class="btn btn-outline-warning">Reset</button>
      </div>
    </form>
  </div>
</body>
</html>