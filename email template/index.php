<!DOCTYPE html>
<html>
<head>
	<title>Reponsive Email</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
	

	<style type="text/css">
		body {
			margin: 0%;
			padding: 0%;
		}

		.outer-panel {
			width: 60%;
			padding: 0% 7%;
			background-color: rgba(0, 0, 0, 0.059);
		}

		.container {
			display: flex;
			justify-content: center;
		}

		.inner-panel {
			background-color: rgb(255, 255, 255, 0.55);
			box-shadow: 1px 2px 10px rgba(0, 0, 0, 0.34);
		}

		p {
			font-size: 12px;
		}

		button {
			font-size: 13px !important;
		}

		.circle {
			width: 40px;
			height: 40px;
			border-radius: 100px;
			margin: 2%;
			border: 1px solid rgba(0, 0, 0, 0.55);
			text-align: center;
			padding: 12px;
		}

		@media only screen and (max-device-width: 425px) {
			.outer-panel {
				width: 100%;
				padding: 0% 3%;
			}

			.title-text {
				padding-top: 10%;
			}

			.circle {
				border : 0px;
			}
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="outer-panel text-center rounded">
			<i class="far fa-bookmark p-2"></i>

			<div class="inner-panel rounded">
				<div class="row">
					<div class="col-md-12">
						<h6 class="mt-3">Responsive HTML Email Template</h6>
						<p>Download Free Source Code</p>
					</div>
					<div class="col-md-12">
						<img src="image/screenshot.png" alt="image1" width="100%" height="90%">
					</div>

					<div class="col-md-12">
						<p>
							Welcome in our Website.
						</p>
						<button type="button" class="btn btn-warning">Visite</button>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">
						<div class="p-4">
							<i class="far fa-images fa-2x py-4 px-3" style="background-color: lightblue;"></i>
						</div>
					</div>
					<div class="col-md-10">
						<div class="p-4">
							<h6>Responsive Design</h6>
							<p>I hope you create something</p>
						</div>
					</div>

					<div class="col-md-2">
						<div class="p-4">
							<i class="far fa-images fa-2x py-4 px-3" style="background-color: lightblue;"></i>
						</div>
					</div>
					<div class="col-md-10">
						<div class="p-4">
							<h6>Responsive Design</h6>
							<p>I hope you create something</p>
						</div>
					</div>

					<div class="col-md-12 p-4">
						<h6>Have your any Question?</h6>
						<a href="#" style="font-size: 12px">arnab@gmail.com</a>
					</div>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-12">
					<footer style="padding: 3% 20%">
						<div class="d-flex flex-row px-4">
							<i class="fab fa-facebook-f circle"></i>
							<i class="fab fa-twitter circle"></i>
							<i class="fab fa-google-plus-g circle"></i>
							<i class="fab fa-instagram circle"></i>
						</div>
						<p>Loren Ipsum is simply dumy text of the printing and typesetting industy.</p>
					</footer>
				</div>
			</div>
		</div>
	</div>

</body>
</html>