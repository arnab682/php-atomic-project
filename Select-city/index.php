<!DOCTYPE html>
<html>
<head>
	<title>Select City</title>
</head>
<body>

	<section>
		<h1>Multiple Select Option</h1>
		<p></p>
		<form action="select-city-process.php" method="post">
			<fieldset>
				<legend>What is your city?</legend>
				
				<select name="city">
					<option value="">Slect your city</option>
					<option value="0">Dhaka</option>
					<option value="1">Chittagong</option>
					<option value="2">Cox's Bazar</option>
					<option value="3">Fani</option>
				</select>

				<button submit>Submit</button>
			</fieldset>
		</form>
	</section>

</body>
</html>