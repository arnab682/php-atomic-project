<!DOCTYPE html>
<html>
<head>
	<title>Toggle</title>
</head>
<body>

	<section>
		<h1>Toggle Yes/No</h1>
		<p>- a Variation in data management</p>
		<form action="radio-toggle-process.php" method="post">
			<fieldset>
				<legend>Toggle- Yes/No</legend>
				
				<input type="radio" name="toggle" id="toggle_yes" value="1" checked="">
				<label for="toggle_yes">Yes</label>
				
				<input type="radio" name="toggle" id="toggle_no" value="0">
				<label for="toggle_no">No</label>

				
				<button submit>Submit</button>
			</fieldset>
		</form>
	</section>

</body>
</html>