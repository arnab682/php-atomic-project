<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
</head>
<body>

	<section>
		<h1>User Contact Form</h1>
		<p>Usually it is user profile management</p>
		<form action="radio-preferred-contact-process.php" method="post">
			<fieldset>
				<legend>What is your contact</legend>
				
				<input type="radio" name="contact" id="email" value="email" checked="">
				<label for="email">Email</label>
				
				<input type="radio" name="contact" id="phone" value="phone">
				<label for="phone">Phone</label>

				<input type="radio" name="contact" id="sms" value="sms">
				<label for="sms">SMS</label>

				<button submit>Submit</button>
			</fieldset>
		</form>
	</section>

</body>
</html>