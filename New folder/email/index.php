<!DOCTYPE html>
<html>
<head>
	<title>Email Subscriber</title>

	<!-- Brootstrap 4 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		#header {
			width: 100%;
			padding: 10px;
			text-align: center;
			margin-top: 2px;
			margin-bottom: 2px;
			background-color: green;
		}

		#header h1{
			color: white;
		}

		.scheduler-border {
                border: 1px groove #6CB5F0 !important;
                padding: 0 1.4em 1.4em 1.4em !important;
                margin: 0 0 1.5em 0 !important;
                -webkit-box-shadow:  0px 0px 0px 0px #1dd5ff;
                box-shadow:  0px 0px 0px 0px #2ac9ff;

        }

        .scheduler-bordered {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border:1px solid #4499F9;
            border-radius: 15px;
            background: #4499F9;
            color: #FFFFFF;
        }
	</style>
</head>
<body>
	<div class="container" >
		<div id="header">
	        <h1>Email Subscriber</h1>
	    </div>

	    <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> Buyers </legend>
            <form action="">
                <div class="form-group">
                    <label>Mother ID :</label>
                    <input name="mother_id" type="text" class="form-control"  placeholder="Enter Mother ID">
                </div>
                <div class="form-group">
                    <label>Buyer Name :</label>
                    <input name="buyer_name" type="text" class="form-control" placeholder="Enter Buyer Name">
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
                
        </fieldset>
	</div>
</body>
</html>