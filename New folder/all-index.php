<!DOCTYPE html>
<html>
<head>
	<title>Atomic Project</title>

	<!-- Brootstrap 4 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    

	<style type="text/css">
		#header {
			width: 100%;
			padding: 10px;
			text-align: center;
			margin-top: 2px;
			margin-bottom: 10px;
			background-color: green;
		}

		#header h1{
			color: white;
		}

		.scheduler-border {
                border: 1px groove #6CB5F0 !important;
                padding: 0 1.4em 1.4em 1.4em !important;
                margin: 0 0 1.5em 0 !important;
                -webkit-box-shadow:  0px 0px 0px 0px #1dd5ff;
                box-shadow:  0px 0px 0px 0px #2ac9ff;

        }

        .scheduler-bordered {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border:1px solid #4499F9;
            border-radius: 15px;
            background: #4499F9;
            color: #FFFFFF;
        }
	</style>
</head>
<body>
	<div class="container" >
		<div id="header">
	        <h1>Atomic Project</h1>
	    </div>
        
	    <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> Email Subscriber </legend>
            <form action="" method="get">
                <div class="form-group">
                    <label>Email :</label>
                    <input name="email" type="text" class="form-control"  placeholder="Enter Your Email">
                </div>
                <button type="submit" class="btn btn-success">Subscrib</button>
            </form>
            <?php
                print_r($_GET);
                echo "<br>";
                $email = null;
                if(array_key_exists('email', $_GET) && !empty(htmlspecialchars(trim($_GET['email'])))){
                    echo("Email is- ".$_GET['email']);
                }
                else{echo "no";}
            ?>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> Password </legend>
            <form action="" method="post">
                <div class="form-group">
                    <label>Password :</label>
                    <input name="password" type="password" class="form-control"  placeholder="Enter Your Password">
                </div>
                <button type="submit" class="btn btn-success">Just OK!</button>
            </form>
            <?php
               // print_r($_POST);
                echo "<br>";
                $password = null;
                if(array_key_exists('password', $_POST)){
                    
                    echo("<br>Password is- ".$_POST['password']);

                    echo("<br>Password is md5- ".md5($_POST['password']));
                    echo("<br>Password is sha1- ".sha1($_POST['password']));


                    $fileResource = fopen('password.txt','w+');
                    fwrite($fileResource, $_POST['password']);


                    echo("<br>Password is md5 file- ".md5_file("password.txt"));
                    echo("<br>Password is sha1 file- ".sha1_file("password.txt"));
                }
                else{echo "no";}

            ?>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> Gender </legend>
            <form action="" method="get">
                <div class="form-group">
                    <label>Gender :</label>

                      <input type="radio" name="gender" value="1">Male
                      <input type="radio" name="gender" value="0">Female
                        
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
            <?php
                print_r($_GET);
                echo "<br>";
                $gender = null;
                if(array_key_exists('gender', $_GET)){
                    if ($_GET['gender']==1) {
                       echo("Gender is- Male");
                    } else {
                        echo("Gender is- Female");
                    }
                }
                else{echo "no";}
            ?>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> Message </legend>
            <form action="#" method="get">
                <label>Your Message :</label>
                <div class="form-group">
                    
                      <textarea rows="2" placeholder="Your Message..." name="message"></textarea>
                      
                        
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
            <?php
                print_r($_GET);
                echo "<br>";
                $message = null;
                if(array_key_exists('message', $_GET) && !empty(trim($_GET['message']))){

                    echo "Your Message is- ".$_GET['message'];
                }
                else{echo "no";}
            ?>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-bordered"> reCaptcha </legend>
            <form action="#" method="post">
                <label>Re-Captche :</label>
                <div class="form-group">
                    
                      
                
                 <div class="g-recaptcha" data-sitekey="6LebReQUAAAAAEypbuVxfLgjZPv4lNOSwfr9u_Ec"></div>
      
                      
                        
                </div>
                <button type="submit"  class="btn btn-success">Submit</button>
            </form>
            <?php
                //print_r($_POST);
                echo "<br>";
                $message = null;
                if(array_key_exists('g-recaptcha-response', $_POST)){

                    echo "Your Message is- OK";
                }
                else{echo "no";}

               
            ?>
        </fieldset>
	</div>

    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</body>
</html>


