<!DOCTYPE html>
<html>
<head>
	<title>Gender</title>
</head>
<body>

	<section>
		<h1>Collect User's Gender</h1>
		<p>Usually it is user profile management</p>
		<form action="radio-gender-process.php" method="post">
			<fieldset>
				<legend>What is your gender</legend>
				
				<input type="radio" name="gender" id="male" value="1" checked="">
				<label for="male">Male</label>
				
				<input type="radio" name="gender" id="female" value="2">
				<label for="female">Female</label>

				<input type="radio" name="gender" id="other" value="3">
				<label for="other">Other</label>

				<button submit>Submit</button>
			</fieldset>
		</form>
	</section>

</body>
</html>