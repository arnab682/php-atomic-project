<!DOCTYPE html>
<html>
<head>
	<title>Select Multiple Language</title>
</head>
<body>

	<section>
		<h1>Multiple Select Option</h1>
		<p></p>
		<form action="select-multiple-languages-process.php" method="post">
			<fieldset>
				<legend>What are the languages you can talk?</legend>
				
				<select name="languages[]" multiple="">
					
					<option value="Bengli">Bengli</option>
					<option value="English">English</option>
					<option value="Arabic">Arabic</option>
					<option value="African">African</option>
				</select>

				<button submit>Submit</button>
			</fieldset>
		</form>
	</section>

</body>
</html>